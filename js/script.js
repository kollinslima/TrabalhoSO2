/*!
 * Start Bootstrap - Freelancer Bootstrap Theme (http://startbootstrap.com)
 * Code licensed under the Apache License v2.0.
 * For details, see http://www.apache.org/licenses/LICENSE-2.0.
 */


// jQuery for page scrolling feature - requires jQuery Easing plugin
$(function() {
    $('body').on('click', '.page-scroll a', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top
        }, 1500, 'easeInOutExpo');
        event.preventDefault();
    });
});

// Floating label headings for the contact form
$(function() {
    $("body").on("input propertychange", ".floating-label-form-group", function(e) {
        $(this).toggleClass("floating-label-form-group-with-value", !! $(e.target).val());
    }).on("focus", ".floating-label-form-group", function() {
        $(this).addClass("floating-label-form-group-with-focus");
    }).on("blur", ".floating-label-form-group", function() {
        $(this).removeClass("floating-label-form-group-with-focus");
    });
});

// Highlight the top nav as scrolling occurs
$('body').scrollspy({
    target: '.navbar-fixed-top'
})

// Closes the Responsive Menu on Menu Item Click
$('.navbar-collapse ul li a').click(function() {
    $('.navbar-toggle:visible').click();
});


function loadXMLDoc() {
  xmlhttp = new XMLHttpRequest();
  xmlhttp.open("GET", "xml/data.xml", true);
  xmlhttp.send();
}

function changeLanguage(xml, lang){
     var xmlDoc = xml.responseXML;
     var l;
     
     if(lang == 0) l = "pt";
     else l = "en";
     
     //Titulo e Intro
     txt = xmlDoc.getElementsByTagName(l)[0].getElementsByTagName("titulo")[0].innerHTML;
     $("title, a.navbar-brand, .name").text(txt);
     txt = xmlDoc.getElementsByTagName(l)[0].getElementsByTagName("subtitulo")[0].innerHTML;
     $(".skills").text(txt);
     txt = xmlDoc.getElementsByTagName(l)[0].getElementsByTagName("img")[0].innerHTML;
     $(".intro-text > img").attr("src","img/"+l+"/"+txt);
     
     //Sections Links
     $( "li.page-scroll" ).each(function( index) {
        txt = xmlDoc.getElementsByTagName(l)[0].getElementsByTagName("secao")[index].getElementsByTagName("titulo")[0].innerHTML;
        $(this).find("a").text(txt);
     });
     
     $( "div.xmlIn.intro" ).each(function(index) {
        $(this).find("p").each(function(i){
            txt = xmlDoc.getElementsByTagName(l)[0].getElementsByTagName("secao")[0].getElementsByTagName("slide")[index].getElementsByTagName("texto")[i].innerHTML;
            $(this).text(txt);
        });
        $(this).find("img").each(function(i){
            txt = xmlDoc.getElementsByTagName(l)[0].getElementsByTagName("secao")[0].getElementsByTagName("slide")[index].getElementsByTagName("img")[i].innerHTML;
            $(this).attr("src","img/"+l+"/"+txt);
        });
     });
     
     $( "div.xmlIn.mutual" ).each(function(index) {
        $(this).find("p").each(function(i){
            txt = xmlDoc.getElementsByTagName(l)[0].getElementsByTagName("secao")[1].getElementsByTagName("slide")[index].getElementsByTagName("texto")[i].innerHTML;
            $(this).text(txt);
        });
        $(this).find("img").each(function(i){
            txt = xmlDoc.getElementsByTagName(l)[0].getElementsByTagName("secao")[1].getElementsByTagName("slide")[index].getElementsByTagName("img")[i].innerHTML;
            $(this).attr("src","img/"+l+"/"+txt);
        });
     });
     
     $( "div.xmlIn.condition" ).each(function(index) {
        $(this).find("p").each(function(i){
            txt = xmlDoc.getElementsByTagName(l)[0].getElementsByTagName("secao")[2].getElementsByTagName("slide")[index].getElementsByTagName("texto")[i].innerHTML;
            $(this).text(txt);
        });
        $(this).find("img").each(function(i){
            txt = xmlDoc.getElementsByTagName(l)[0].getElementsByTagName("secao")[2].getElementsByTagName("slide")[index].getElementsByTagName("img")[i].innerHTML;
            $(this).attr("src","img/"+l+"/"+txt);
        });
     });
     
     $( "div.xmlIn.details" ).each(function(index) {
        $(this).find("p").each(function(i){
            txt = xmlDoc.getElementsByTagName(l)[0].getElementsByTagName("secao")[3].getElementsByTagName("slide")[index].getElementsByTagName("texto")[i].innerHTML;
            $(this).text(txt);
        });
        $(this).find("img").each(function(i){
            txt = xmlDoc.getElementsByTagName(l)[0].getElementsByTagName("secao")[3].getElementsByTagName("slide")[index].getElementsByTagName("img")[i].innerHTML;
            $(this).attr("src","img/"+l+"/"+txt);
        });
     });
     
     $( "div.xmlIn.main" ).each(function(index) {
        $(this).find("p").each(function(i){
            txt = xmlDoc.getElementsByTagName(l)[0].getElementsByTagName("secao")[4].getElementsByTagName("slide")[index].getElementsByTagName("texto")[i].innerHTML;
            $(this).text(txt);
        });
     });
     
      $( "div.xmlIn.download" ).each(function(index) {
        $(this).find("p").each(function(i){
            txt = xmlDoc.getElementsByTagName(l)[0].getElementsByTagName("secao")[5].getElementsByTagName("slide")[index].getElementsByTagName("texto")[i].innerHTML;
            $(this).text(txt);
        });
     });
     
}

function getXML(lang){
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (xhttp.readyState == 4 && xhttp.status == 200) {
            changeLanguage(xhttp, lang);
        }
    };
    xhttp.open("GET", "xml/data.xml", true);
    xhttp.send();

}

    $(".lang.br").click(function() {

        getXML(0);
    });
    $(".lang.en").click(function() {
        getXML(1);
    });

$(document).ready(function(){
    var count = 0;
    var xmlhttp;
    
    $("section").each(function() {
        if (count%2==0)
            $(this).attr("class", "");
        else
            $(this).attr("class", "success");
        
        count++;
    });
    
    getXML(0);
    

    
   
    
});
